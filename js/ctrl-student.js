app.controller('studentCtrl', function ($scope) {
    $scope.init = function () {
        getTable({c: 'Student', m: 'getAll'});
        getRoomList();
    };

    function getTable(data) {
        dt = data || {};
        $('#datatable tfoot').html("");
        $('#datatable').dataTable({
            ajax: {
                url: 'src/api.php',
                type: 'POST',
                data: dt
            },
            columns: [
                {'data': 'id', title: 'ID'},
                {'data': 'code', title: 'รหัสประจำตัว'},
                {'data': 'name', title: 'ชื่อ'},
                {'data': 'checked', title: 'มา'},
                {
                    'title': 'Status', defaultContent: '',
                    render: function (data, type, row, meta) {
                        return '<button class="btn btn-warning btn-sm edit" data-c="Student" data-id="' + row.id + '"><i class="fa fa-pencil"></i></button>'+
                            '<button class="btn btn-danger btn-sm delete" data-c="Student" data-id="' + row.id + '"><i class="fa fa-trash"></i></button>';
                    }
                }
            ],
            dataSrc: 'data', //key for data loop
            responsive: true,
            bDestroy: true, //destroy when new search
            dom: "lftiBp", //l:lengthBox, f:findBox, B:Button Action, p:paginate, t:table, i:information
            buttons: [
                {
                    extend: "copy",
                    className: "btn-sm"
                },
                {
                    extend: "csv",
                    className: "btn-sm"
                },
                {
                    extend: "excel",
                    className: "btn-sm"
                },
                {
                    extend: "pdfHtml5",
                    className: "btn-sm"
                },
                {
                    extend: "print",
                    className: "btn-sm"
                }
            ],
            // order: [[0, "desc"]],
            createdRow: function (row, data, index) {
//                console.log(data);
                // switch ( data['WinLossStatus']) {
                //     case 'P': $('td', row).eq(9).addClass('text-warning').text('Pending'); break;
                //     case 'W': $('td', row).eq(9).addClass('text-success').text('Win'); break;
                //     case 'L': $('td', row).eq(9).addClass('text-danger').text('Lose'); break;
                // }
            }
        });
    }

    $scope.submit = function () {
        if (!confirm('ยืนยัน?')) return;
        var form = objectifyForm($('#form-student'));

        ajax('src/api.php', function (res) {
            if (res.success) {
                alert('เพิ่มข้อมูลสำเร็จ');
                return location.reload();
            }
            alert(res.data);
        }, {
            c: 'Student',
            m: form.id == '0' ? 'insert' : 'update',
            data: form
        });
    };
});

$(document).on('click', '#student-page .edit', function () {
    ajax('src/api.php', function (res) {
        $('[name="id"]').val(res.data.id);
        $('[name="code"]').val(res.data.code);
        $('[name="name"]').val(res.data.name);
        $('[name="room_id"]').val(res.data.room_id);
        $('#studentCreateModal').modal('show');
    }, {
        c: 'Student',
        m: 'load',
        data: {id: $(this).data('id')}
    });
});

$(document).on('hide.bs.modal', '#studentCreateModal', function () {
    $('[name="id"]').val('0');
    $('#form-student')[0].reset();
});
$(document).on('show.bs.modal', '#studentCreateModal', function () {
    $('#studentCreateModal .submit-add, #studentCreateModal .submit-edit').hide();
    if ($('[name="id"]').val() == '0') {
        $('#studentCreateModal .modal-title').text('เพิ่มรายชื่อ');
        $('#studentCreateModal .submit-add').show();
    } else {
        $('#studentCreateModal .modal-title').text('แก้ไขรายชื่อ');
        $('#studentCreateModal .submit-edit').show();
    }
});
