app.controller('roomCtrl', function ($scope) {
    console.log('aaa');
    $scope.init = function () {
        getTable({c: 'Room', m: 'getAll'});
    };

    $scope.submit = function () {
        if (!confirm('ยืนยัน?')) return;
        var form = objectifyForm($('#form-room'));
        ajax('src/api.php', function (res) {
            console.log(res);
            if (res.success) {
                alert('success');
                location.reload();
            } else {
                alert(res.code);
            }
        }, {
            c: 'Room',
            m: form.id == '0' ? 'insert' : 'update',
            data: form
        })
    };

    function getTable(data) {
        dt = data || {};
        $('#datatable tfoot').html("");
        $('#datatable').dataTable({
            ajax: {
                url: 'src/api.php',
                type: 'POST',
                data: dt
            },
            columns: [
                {'data': 'id', title: 'ID'},
                {'data': 'room', title: 'ห้อง'},
                {'data': 'length', title: 'จำนวนนักเรียน'},
                {'title': 'Status', defaultContent: '',
                    render: function (data, type, row, meta) {
                        return '<button class="btn btn-warning btn-sm edit" data-c="Room" data-id="' + row.id + '"><i class="fa fa-pencil"></i></button>'+
                            '<button class="btn btn-danger btn-sm delete" data-c="Room" data-id="' + row.id + '"><i class="fa fa-trash"></i></button>';
                    }
                }
            ],
            dataSrc: 'data', //key for data loop
            responsive: true,
            bDestroy: true, //destroy when new search
            dom: "lftiBp", //l:lengthBox, f:findBox, B:Button Action, p:paginate, t:table, i:information
            buttons: [
                {
                    extend: "copy",
                    className: "btn-sm"
                },
                {
                    extend: "csv",
                    className: "btn-sm"
                },
                {
                    extend: "excel",
                    className: "btn-sm"
                },
                {
                    extend: "pdfHtml5",
                    className: "btn-sm"
                },
                {
                    extend: "print",
                    className: "btn-sm"
                }
            ],
            // order: [[0, "desc"]],
            createdRow: function (row, data, index) {
//                console.log(data);
                // switch ( data['WinLossStatus']) {
                //     case 'P': $('td', row).eq(9).addClass('text-warning').text('Pending'); break;
                //     case 'W': $('td', row).eq(9).addClass('text-success').text('Win'); break;
                //     case 'L': $('td', row).eq(9).addClass('text-danger').text('Lose'); break;
                // }
            }
        });
    }
});

$(document).on('click', '#room-page .edit', function () {
    ajax('src/api.php', function (res) {
        $('[name="id"]').val(res.data.id);
        $('[name="room"]').val(res.data.room);
        $('#room-create-modal').modal('show');
    }, {
        c: 'Room',
        m: 'load',
        data: {id: $(this).data('id')}
    });
});

$(document).on('hide.bs.modal', '#room-create-modal', function () {
    $('[name="id"]').val('0');
    $('#form-room')[0].reset();
});
$(document).on('show.bs.modal', '#room-create-modal', function () {
    $('#room-create-modal .submit-add, #room-create-modal .submit-edit').hide();
    if ($('[name="id"]').val() == '0') {
        $('#room-create-modal .modal-title').text('เพิ่มห้องเรียน');
        $('#room-create-modal .submit-add').show();
    } else {
        $('#room-create-modal .modal-title').text('แก้ไขห้องเรียน');
        $('#room-create-modal .submit-edit').show();
    }
});
