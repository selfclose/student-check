//http://l-lin.github.io/angular-datatables/archives/#!/zeroConfig

var app = angular.module('studentApp', ['ngRoute']);
app.config(function($routeProvider) {
    $routeProvider
        .when("/", {
            templateUrl : "html/index.html"
        })
        .when("/room", {
            templateUrl : "html/room.html",
            controller: 'roomCtrl'
        })
        .when("/student", {
            templateUrl : "html/student.html",
            controller: 'studentCtrl'
        })
        .when("/checking", {
            templateUrl : "html/checking.html",
            controller: 'checkingCtrl'
        })
        .when("/blue", {
            templateUrl : "blue.htm"
        })
        .otherwise({redirectTo:'/'});
});

$(document).ready(function () {

});

$(document).on('click', '#datatable .delete', function () {
    if (!confirm('ยืนยันการลบ')) return;
    ajax('src/api.php', function (res) {
        alert('Deleted!');
        location.reload();
    }, {
        c: $(this).data('c'),
        m: 'delete',
        data: {id: $(this).data('id')}
    })
});

function getRoomList() {
    ajax('src/api.php', function (res) {
        if (res.success) {
            $('#room-id').empty();
            for (var i=0; i<res.data.length; i++) {
                $('#room-id').append('<option value="'+res.data[i]['id']+'">'+res.data[i]['room']+'</option>');
            }
        }
    }, {
        c: 'Room',
        m: 'getAll'
    });
}

var servIP = '';
function ajax(path, callback, data) {
    $.ajax({
        url: servIP + path,
        xhrFields: {withCredentials: true},
        type: data === undefined ? 'GET' : 'POST',
        data: data
    }).done(function (res) {
        return callback(res);
    });
}

function ajaxFormData(path, form, callback) {
    var formData = new FormData(form);
    $.ajax({
        type: 'POST',
        xhrFields: {withCredentials: true},
        url: servIP + path,
        data: formData,
        cache: false,
        contentType: false,
        processData: false,
        success: function (data) {
            console.log("success");
            return callback(data);
        },
        error: function (data) {
            console.log("error");
            console.log(data);
        }
    });
}

function objectifyForm(formObject) {//serialize data function
    var formArray = formObject.serializeArray();
    var returnArray = {};
    for (var i = 0; i < formArray.length; i++) {
        if (formArray[i]['value'] !== '')
            returnArray[formArray[i]['name']] = formArray[i]['value'];
    }
    return returnArray;
}
