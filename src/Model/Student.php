<?php
namespace Model;

class Student {
    static $table = 'student';
    public $checked = 0;

    public function __construct()
    {

    }

    public function insert($data)
    {
        if(!preg_match("/^.{3,12}$/",$data['name'])) return json_error('รหัสนักศึกษาไม่ถูกต้อง', 'INPUT_FORMAT_ERROR');
        if ($this->isExist($data['code'])) return json_error('รหัสนักศึกษานี้ มีแล้ว', 'DUPLICATE_CODE');
        if(!preg_match("/^.{3,20}$/",$data['name'])) return json_error('ชื่อยาว 3-20 อักษร', 'INPUT_FORMAT_ERROR');
        if (!\R::load(Room::$table, $data['room_id'])) return json_error('ห้องนี้ไม่มีอยู่', 'ROOM_NOT_EXIST');

        $t = \R::getRedBean()->dispense(self::$table);
        $t->code = $data['code'];
        $t->name = $data['name'];
        $t->room_id = $data['room_id'];
        $t->checked = $this->checked;
        $t->created_at = date('Y-m-d H:i:s');
        $t->updated_at = date('Y-m-d H:i:s');
        return json_success(\R::store($t));
    }

    public function update($data)
    {
        if ($t = \R::load(self::$table, $data['id'])) {
            if ($t->id == 0) return json_error('ID not exist', 'UPDATE_ERROR_ID_NOT_EXIST');
            $t->code = $data['code'];
            $t->name = $data['name'];
            $t->room_id = $data['room_id'];
            $t->updated_at = date('Y-m-d H:i:s');
            return json_success(\R::store($t));
        }
        return json_error('ไม่มี ID', 'UPDATE_ERROR');
    }

    public function delete($data)
    {
        \R::trash(self::$table, $data['id']);
        return json_success($data['id'], 'RECORD_DELETED');
    }

    public function load($data)
    {
        if ($res = \R::load(self::$table, $data['id'])) {
            return json_success(\R::exportAll($res)[0]);
        }
        return json_error([], 'NO_DATA');
    }

    public function getAll()
    {
        $res = [];
        foreach (\R::exportAll(\R::findAll(self::$table)) as $item) {
            array_push($res, array_merge($item, ['room' => \R::count(Student::$table, 'room_id = ?', [$item['id']])]));
        }
        return json_success($res);

//        return json_success(\R::exportAll(\R::findAll(self::$table)));
    }

    public function isExist($code)
    {
        return \R::count(self::$table, ' code = ?', [$code]);
    }
}