<?php
class Checking
{
    static $table = 'checking';

    public function insert($data)
    {
        $t = \R::getRedBean()->dispense(self::$table);
        $t->student_id = $data['id'];
        $t->status = $data['status']; //'C':'Came', 'A':Absent, 'B':'Business'
        $t->created_at = date('Y-m-d H:i:s');
        $t->updated_at = date('Y-m-d H:i:s');

    }
}
