<?php
namespace Model;

class Room {
    static $table = 'room';

    public function insert($data)
    {
        if(!preg_match("/^.{2,10}$/",$data['room'])) return json_error('กรุณากรอกข้อมูลให้ถูกต้อง', 'INPUT_FORMAT_ERROR');

        $r = \R::getRedBean()->dispense(self::$table);
        $r->room = $data['room'];
        if ($res = \R::store($r)) {
            return json_success($res, 'ROOM_INSERTED');
        }
        return json_error([],'ROOM_INSERT_ERROR');
    }

    public function update($data)
    {
        if ($t = \R::load(self::$table, $data['id'])) {
            if ($t->id == 0) return json_error('ID not exist', 'UPDATE_ERROR_ID_NOT_EXIST');
            $t->room = $data['room'];
            return json_success(\R::store($t));
        }
        return json_error('ไม่มี ID', 'UPDATE_ERROR');
    }

    public function getAll()
    {
        $res = [];
        foreach (\R::exportAll(\R::findAll(self::$table)) as $item) {
            array_push($res, array_merge($item, ['length' => \R::count(Student::$table, 'room_id = ?', [$item['id']])]));
        }
        return json_success($res);
    }

    public function delete($data)
    {
        \R::trash(self::$table, $data['id']);
        return json_success($data['id'], 'RECORD_DELETED');
    }

    public function load($data)
    {
        if ($res = \R::load(self::$table, $data['id'])) {
            return json_success(\R::exportAll($res)[0]);
        }
        return json_error([], 'NO_DATA');
    }
}
