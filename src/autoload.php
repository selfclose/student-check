<?php
include_once __DIR__.'\..\config\db.php';
include_once __DIR__.'\vendors\rb5\rb.php';
R::setup( 'sqlite:../db/database.sqlite3');
if (!R::testConnection()) {
    json_error('database connection error', 'DB_ERROR');
}

function autoload($className)
{
    $className = ltrim($className, '\\');
    $fileName  = '';
    $namespace = '';
    if ($lastNsPos = strrpos($className, '\\')) {
        $namespace = substr($className, 0, $lastNsPos);
        $className = substr($className, $lastNsPos + 1);
        $fileName  = str_replace('\\', DIRECTORY_SEPARATOR, $namespace) . DIRECTORY_SEPARATOR;
    }
    $fileName .= str_replace('_', DIRECTORY_SEPARATOR, $className) . '.php';

    if (is_file($fileName)) require_once $fileName;
}
spl_autoload_register('autoload');

function return_json($code = 'ERROR', $msg = 'Unknown error') {
    die(json_encode(['code' => $code, 'msg' => $msg]));
}
function json_success($data = '', $code = 'SUCCESS') {
    die(json_encode(['success' => true, 'code' => $code, 'data' => $data]));
}
function json_error($data = '', $code = 'ERROR') {
    die(json_encode(['success' => false, 'code' => $code, 'data' => $data]));
}
function json_redbean_success($code = 'SUCCESS', $bean) {
    json_success($code, \R::exportAll($bean));
}
