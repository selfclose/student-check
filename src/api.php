<?php

header('Content-Type: application/json');
header("Access-Control-Allow-Origin: *");

date_default_timezone_set ( 'Asia/Bangkok');
//if (!$_POST['method']) {
//    die(json_encode(['code' => 'NO_METHOD', 'msg' => 'No data']));
//}

require_once __DIR__.'\autoload.php';

$class_name =  '\Model\\'.$_POST['c'];
$method_name = $_POST['m'];
if (!class_exists($class_name)) json_error('C type not exist', 'C_ERROR');
$model = new $class_name;
if (!method_exists($model, $method_name)) json_error('M type not exist', 'M_ERROR');
$model->$method_name($_POST['data']);


/*
$param = $_POST['param'];

switch ($_POST['method']) {
    case 'student_add':
        include_once __DIR__.'/Model/Student.php';
        $student = new Student();
        $student->code = $_POST['code'];
        $student->name = $_POST['name'];
        $student->room = $_POST['room'];
        if ($res = $student->insert()) {
            json('SUCCESS', 'Student inserted ID: '.$res);
        } else {
            json('INSERT_ERROR', 'Cannot add student');
        }
        break;
    case 'student_lists':
        include_once __DIR__.'/Model/Student.php';
        $student = new Student();


    default: return_json('METHOD_ERROR', 'Method name incorrect');
}*/

